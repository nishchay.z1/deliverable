package com.lovedeep.companyproject.model;

import org.springframework.context.annotation.Bean;

/**
 * Created by T98021 on 9/4/2019.
 */

public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private String department;
    private String designation;

    public Employee() {
    }

    public Employee(int id, String firstName, String lastName, String department, String designation) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
        this.designation = designation;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public String getDepartment() {
        return department;
    }

    public String getDesignation() {
        return designation;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}

 