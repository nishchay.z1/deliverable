package com.lovedeep.companyproject.model;

/**
 * Created by T98021 on 9/4/2019.
 */
public class Bonus
{
    int id;
    int percentage;

    public Bonus(int id, int percentage)
    {
        this.id = id;
        this.percentage = percentage;
    }

    public int getId() {
        return id;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }
}

 