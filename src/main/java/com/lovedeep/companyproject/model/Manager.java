package com.lovedeep.companyproject.model;

/**
 * Created by T98021 on 9/4/2019.
 */
public class Manager {

    int id;
    int noOfEmployees;

    public Manager(int id, int noOfEmployees)
    {
        this.id = id;
        this.noOfEmployees = noOfEmployees;
    }

    public int getId() {
        return id;
    }

    public int getNoOfEmployees() {
        return noOfEmployees;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNoOfEmployees(int noOfEmployees) {
        this.noOfEmployees = noOfEmployees;
    }
}
