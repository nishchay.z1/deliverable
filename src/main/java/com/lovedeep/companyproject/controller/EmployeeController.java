package com.lovedeep.companyproject.controller;

import com.lovedeep.companyproject.model.Employee;
import com.lovedeep.companyproject.repository.EmployeeRepository;
import com.lovedeep.companyproject.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping("/employee")
    public Map<?, ?> part1call(){
        return employeeService.printPart1();

    }

    @GetMapping("/employee1")
    public Map<?, ?> part1cal2(){
        return employeeService.printPart2();

    }

    @GetMapping("/employee2")
    public Map<?, ?> part1cal3(){
        return employeeService.printPart3();

    }




}
