package com.lovedeep.companyproject.service;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.lovedeep.companyproject.model.Bonus;
import com.lovedeep.companyproject.model.Employee;
import com.lovedeep.companyproject.model.Manager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import com.lovedeep.companyproject.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

/**
 * Created by T98021 on 9/4/2019.
 */

@Service
public class EmployeeService
{



    Employee employee=new Employee();



    Map<Integer, Employee> map = new HashMap<Integer, Employee>();
    List<Manager> managerList = new ArrayList<Manager>();
    List<Bonus> bonusList = new ArrayList<Bonus>();
    public EmployeeService()
    {
        readEmployeeFile();
        readManagerFile();
        readBOnusFile();
    }

    public void readEmployeeFile()
    {
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader("src/main/resources/employee.txt"));
            String line = "";
            while((line = reader.readLine()) != null)
            {
                String[] array = line.split(",");
                int id = Integer.parseInt(array[0]);
                map.put(id, new Employee(id, array[1], array[2], array[3], array[4]));
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void readManagerFile()
    {
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader("src/main/resources/manager.txt"));
            String line = "";
            while((line = reader.readLine()) != null)
            {
                String[] array = line.split(",");
                int id = Integer.parseInt(array[0]);
                int count = Integer.parseInt(array[1]);
                managerList.add(new Manager(id, count));
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void readBOnusFile()
    {
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader("src/main/resources/bonus.txt"));
            String line = "";
            while((line = reader.readLine()) != null)
            {
                String[] array = line.split(",");
                int id = Integer.parseInt(array[0]);
                int bonus = Integer.parseInt(array[1]);
                bonusList.add(new Bonus(id, bonus));
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public Map<?, ?>printPart1()
    {
        Map map1=new HashMap();
        System.out.println("--------------------------------");
        for(Manager m : managerList)
        {
            System.out.println(map.get(m.getId()).getFirstName() + " " + map.get(m.getId()).getLastName() + " = " + m.getNoOfEmployees());
            map1.put(map.get(m.getId()).getFirstName() + " " + map.get(m.getId()).getLastName(),m.getNoOfEmployees());
        }
        System.out.println("--------------------------------");
        return map1;
    }

//    public Map<Integer, Employee> printPart2()
        public Map<?,?> printPart2()

    {
        Map map1=new HashMap();
        System.out.println("--------------------------------");
        for(Bonus m : bonusList)
        {
            System.out.println(map.get(m.getId()).getFirstName() + " " + map.get(m.getId()).getLastName() + " = " + m.getPercentage());
            map1.put(map.get(m.getId()).getFirstName() + " " + map.get(m.getId()).getLastName(), m.getPercentage());

        }




        System.out.println("--------------------------------");
        return map1;
    }

    public Map<?,?> printPart3()
    {
        Map map1=new HashMap();

        for(Bonus m : bonusList)
        {
            if(map.get(m.getId()).getDesignation().compareTo("Manager") == 0) m.setPercentage(m.getPercentage() + 10);
            else if(map.get(m.getId()).getDesignation().compareTo("Manager") != 0 && map.get(m.getId()).getDesignation().compareTo("Intern") != 0) m.setPercentage(m.getPercentage() + 5);
            else m.setPercentage(m.getPercentage() - 2);

        }
        return printPart2();


    }

}

 