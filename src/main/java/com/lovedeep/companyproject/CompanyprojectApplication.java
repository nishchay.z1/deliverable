package com.lovedeep.companyproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompanyprojectApplication {

	public static void main(String[] args) {

		SpringApplication.run(CompanyprojectApplication.class, args);
		System.out.println("\nStarted\n");

	}

}
